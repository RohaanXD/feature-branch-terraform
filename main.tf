provider "aws" {
  region = "us-west-2"
}

resource "aws_instance" "example" {
  ami           = "ami-0c55b159cbfafe1f0"
  instance_type = "t2.micro"

  tags = {
    Name = "${var.instance_name}"
  }
}

resource "aws_route53_record" "example" {
  zone_id = "ZONE_ID"
  name    = "${var.subdomain_name}"
  type    = "A"

  alias {
    name                   = "${aws_instance.example.public_dns}"
    zone_id               = "${aws_instance.example.aws_route53_zone_id}"
    evaluate_target_health = false
  }
}

variable "instance_name" {
  type    = string
  default = "example-instance"
}

variable "subdomain_name" {
  type    = string
  default = "example.yourdomain.com"
}
